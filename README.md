# Final MisPalabras

Práctica final curso 2021-2022 (MisPalabras)

## Datos

* Nombre: Alba Payo Fernández
* Grado: Ingeniería Telemática
* Video parte obligatoria: https://youtu.be/sdZqt6QdvZA
* Video parte opcional: https://youtu.be/BVSPi6ks6r4
* Despliegue: http://albapf.pythonanywhere.com

## Cuenta admin site
albapf/albapf

## Cuenta registrada
* albapf/albapf
* usuario/usuario

## Requirements
* cloudscraper
* BeautifulSoup

## Resumen parte obligatoria
La página de Mi cuenta la he llamado como el nombre de usuario de la persona que se conecta y el índice lo he puesto en la parte de arriba de la página.

## Lista partes opcionales
* En el pie de página he puesto un boton para volver a la página de inicio.
* He implementado un favicon
* El usuario puede salirse y de la cuenta y borrarla
* He añadido la creación de los documentos XML y JSON