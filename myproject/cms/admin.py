from django.contrib import admin
from .models import Palabra, Comentarios, Votos, Urls

# Register your models here.

admin.site.register(Palabra)
admin.site.register(Comentarios)
admin.site.register(Votos)
admin.site.register(Urls)
