from django import forms

class LoginForm(forms.Form):
    user = forms.CharField(label='name', widget=forms.Textarea)
    password = forms.CharField(label='password', widget=forms.Textarea)

class RegisterForm(forms.Form):
    user = forms.CharField(label='name', widget=forms.Textarea)
    password = forms.CharField(label='password', widget=forms.Textarea)
    repeat_password = forms.CharField(label='repeat_password', widget=forms.Textarea)