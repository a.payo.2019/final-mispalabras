import datetime, urllib, operator
from itertools import chain
import random, json, cloudscraper
from urllib.request import urlopen
from xml.etree.ElementTree import parse
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth import logout, login, authenticate
from bs4 import BeautifulSoup
from .models import Palabra, Votos, Comentarios, Urls
from django.core import serializers
from django.template import loader


def more_voted():
    diccionario = {}
    w = Palabra.objects.all()
    for palabra in w:
        votos = Votos.objects.filter(palabra=palabra)
        num_votos = 0
        for voto in votos:
            num_votos += voto.valor
        diccionario[palabra.valor] = num_votos
    ordered_list = sorted(diccionario.items(), reverse=True)[0:10]
    final = []
    for palabra in ordered_list:
        final.append(palabra[0])

    return final

def list_words():
    lista = []
    w = Palabra.objects.all().values_list('valor')
    for palabra in w:
        lista.append(palabra)

    return lista

def length_list_words():
    lista = []
    w = Palabra.objects.all()
    for palabra in w:
        lista.append(palabra)
    lista = len(lista)

    return lista

def random_words():
    lista = list_words()
    if len(lista) != 0:
        random_word = random.choice(lista)
        random_word = str(random_word).split("'")[1]
    else:
        random_word = " "

    return random_word


def new_words(request, page):
    try:
        final = []
        palabras = Palabra.objects.all().values()
        listaord = (sorted(palabras, key=lambda x: x['fecha'], reverse=True))
        for pal in listaord:
            pal2 = Palabra.objects.get(valor=pal['valor'])
            votos = Votos.objects.filter(palabra=pal2).values()
            num_votos = 0
            for voto in votos:
                num_votos += voto['valor']
            dic = {}
            dic['palabra'] = pal2
            dic['votos'] = num_votos
            final.append(dic)
        return final
    except:
        lista = []
        return lista

def def_rae(request, word):
    url = "https://dle.rae.es/" + word
    headers = {'User-Agent': "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    definicion = soup.find("meta", {"property": "og:description"})
    if not definicion["content"].startswith("1"):
        return "Esta palabra no está en la RAE"
    return definicion["content"]


def flickr(request, word):
    try:
        url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + word
        scraper = cloudscraper.create_scraper()
        soup = BeautifulSoup(scraper.get(url).text, 'html.parser')
        urlimagen = soup.find('feed').findAll('entry')[0]
        for a in urlimagen.findAll('link', {'rel': 'enclosure'}, href=True):
            link = a['href']
        return link
    except:
        return "https://apimeme.com/meme?meme=Young-Cardi-B&top=Mi+mam%C3%A1+dice+Que...+&bottom=esta+palabra+no+tiene+imagen+en+Flickr"


def wiki_image(request, word):
    try:
        url = "https://es.wikipedia.org/w/api.php?action=query&titles=" + word + "&prop=pageimages&format=json&pithumbsize=200"
        query = str(urlopen(url).read().decode('utf-8'))
        objeto = json.loads(query)
        url_media = objeto['query']['pages']
        id = str(url_media).split("'")[1]
        url_imagen = url_media[id]['thumbnail']['source']
    except:
        url_imagen = "https://apimeme.com/meme?meme=Young-Cardi-B&top=Mi+mam%C3%A1+dice+Que...+&bottom=esta+palabra+no+tiene+imagen+en+Wikipedia"
    return url_imagen

def wiki_palabra(request, word):
    try:
        url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + word + "&prop=extracts&exintro&explaintext"
        xmldoc = parse(urlopen(url))
        definicion = xmldoc.find('query/pages/page/extract').text
        if not definicion:
            definicion = "Esta palabra no tiene definición en wikipedia"
        return definicion
    except:
        return "Esta palabra no tiene definición en wikipedia"

def embedded_card(request, url):
    try:
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')
        definicion = soup.find("meta", {"property": "og:description"})["content"]
        if definicion is None:
            definicion = soup.find("meta", {"property": "og:title"})["content"]
        if definicion is None:
            definicion = ""
        imagen = soup.find("meta", {"property": "og:image"})["content"]
        if imagen is None:
            imagen = ""
        return definicion, imagen
    except:
        return "", ""

def check_forms(request):
    value = ""
    option = ""
    text = ""
    info = ""
    if request.POST['name'] == "login":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
    elif request.POST['name'] == "word":
        value = "/" + request.POST['valor']
    elif request.POST['name'] == "meme":
        option = request.POST['opcionmeme']
        text = request.POST['texto']
    elif request.POST['name'] == "rae":
        info = "rae"
    elif request.POST['name'] == "Flickr":
        info = "Flickr"
    elif request.POST['name'] == "save":
        info = "save"
    elif request.POST['name'] == "Comentario":
        info = "comentario"
    elif request.POST['name'] == "urls":
        info = "urls"
    elif request.POST['name'] == "Like":
        info = "like"
    elif request.POST['name'] == "Dislike":
        info = "dislike"
    elif request.POST['name'] == "rmvote":
        info = "rmvote"

    return value, option, text, info





def register(request):
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json", Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')
    num_pals = length_list_words()
    rand = random_words()
    cuenta = False
    if request.method == "POST" and (request.POST['name'] == "register"):
        username = request.POST['username']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            email = request.POST['email']
            password = request.POST['password']
            newuser = User.objects.create_user(username, email, password)
            login(request, newuser)
            return redirect('/')
    template = 'forms/registration.html'
    ordered_list = more_voted()
    context = {
        'cuenta': cuenta,
        'more_voted': ordered_list,
        'num_pals': num_pals,
        'rand': rand
    }

    return render(request, template, context)

def logged(request):
    if request.user.is_authenticated:
        logged = 'Logged in as ' + request.user.username
    else:
        logged = 'Not logged in.'

    return HttpResponse(logged)


def delete_user(request):
    usuario = request.user
    logout(request)
    usuario.delete()
    return redirect('/')

def loggout(request):
    logout(request)
    return redirect('/')

def help(request):
    template = 'cms/help.html'
    ordered_list = more_voted()
    num_pals = length_list_words()
    rand = random_words()
    cuenta = False
    context = {
                'cuenta': cuenta,
                'num_pals': num_pals,
                'rand': rand,
                'more_voted': ordered_list}
    return render(request, template, context)

def cuenta(request, user):
    comentarios = Comentarios.objects.filter(usuario=request.user).values()
    urls = Urls.objects.filter(usuario=request.user).values()
    palabras = Palabra.objects.filter(autor=request.user).values()
    num_pals = length_list_words()
    listatotal = list(chain(palabras, comentarios, urls))
    listatotal = (sorted(listatotal, key=lambda x: x['fecha'], reverse=True))
    ordered_list = more_voted()
    rand = random_words()
    template = 'cms/cuenta.html'
    cuenta = True

    if request.method == "POST":
        value = check_forms(request)
        x = value[0]
        if x:
            return redirect(x)

    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')

        elif format == "json":
            data = serializers.serialize("json", Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')

    context = {'cuenta': cuenta,
               'listatotal': listatotal,
               'num_pals': num_pals,
               'rand': rand,
               'more_voted': ordered_list}

    return render(request, template, context)

def words(request, palabra):
    num_pals = length_list_words()
    rand = random_words()
    rae = ""
    Flickr = ""
    try:
        pal = Palabra.objects.get(valor=palabra)
        definicion = pal.definicion
        url_imagen = pal.imagen
        rae = pal.rae
        Flickr = pal.Flickr
        format = request.GET.get('format', None)
        word_exist = True

        if format:
            if format == "xml":
                data = serializers.serialize("xml", Palabra.objects.filter(valor=palabra))
                return HttpResponse(data, content_type='text/xml')

            elif format == "json":
                data = serializers.serialize("json", Palabra.objects.filter(valor=palabra))
                return HttpResponse(data, content_type='text/json')

        if request.method == "POST":
            value = check_forms(request)
            x = value[0]
            opcion = value[1]
            texto = value[2]
            info = value[3]
            if x:
                return redirect(x)

            if opcion:
                pal.Meme = "https://apimeme.com/meme?meme=" + opcion + "&top=" + palabra + "&bottom=" + texto

            if info == "rae":
                pal.rae = def_rae(request, palabra)

            elif info == "Flickr":
                pal.Flickr = flickr(request, palabra)

            elif info == "comentario":
                comen = request.POST['valor']
                c = Comentarios(valor=comen, palabra=pal, usuario=request.user, fecha=datetime.datetime.now())
                c.save()

            elif info == "urls":
                link = request.POST['valor']
                descripcion, imagen_tarjeta = embedded_card(request,link)
                if descripcion and imagen_tarjeta:
                    e = Urls(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now(),
                               descripcion=descripcion, imagen=imagen_tarjeta)
                elif imagen_tarjeta:
                    e = Urls(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now(), imagen=imagen_tarjeta)
                elif descripcion:
                    e = Urls(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now(),
                               descripcion=descripcion)
                else:
                    e = Urls(valor=link, palabra=pal, usuario=request.user, fecha=datetime.datetime.now())
                e.save()

            elif info == "like":
                valor = 1
                v = Votos(valor=valor, palabra=pal, usuario=request.user, fecha=datetime.datetime.now())
                v.save()

            elif info == "dislike":
                valor = -1
                v = Votos(valor=valor, palabra=pal, usuario=request.user, fecha=datetime.datetime.now())
                v.save()

            pal.save()
            return redirect('/' + palabra)

        if pal.rae != "":
            RAE_saved = True

        else:
            RAE_saved = False

        if pal.Flickr != "":
            Flickr_saved = True

        else:
            Flickr_saved = False

        Meme = pal.Meme
        votos = Votos.objects.filter(palabra=pal)
        voted = False

        if request.user.is_authenticated:
            try:
                Votos.objects.get(usuario=request.user, palabra=pal)
                voted = True
            except Votos.DoesNotExist:
                pass
        num_votos = 0
        for vo in votos:
            num_votos += vo.valor
        comentarios_list = Comentarios.objects.filter(palabra=pal)
        enlaces_list = Urls.objects.filter(palabra=pal)

    except Palabra.DoesNotExist:
        comentarios_list = []
        enlaces_list = []
        num_votos = 0
        definicion = wiki_palabra(request, palabra)
        url_imagen = wiki_image(request, palabra)
        Meme = ""
        RAE_saved = False
        Flickr_saved = False
        voted = False
        word_exist = False

        if request.method == "POST":
            value = check_forms(request)
            x = value[0]
            info = value[3]
            if x:
                return redirect(x)
            if info == "save":
                pal = Palabra(valor=palabra, autor=request.user, fecha=datetime.datetime.now(), definicion=definicion,
                              imagen=url_imagen)
                pal.save()
                return redirect('/' + palabra)

    ordered_list = more_voted()
    context = {
        'Flickr': Flickr,
        'rae': rae,
        'RAE_saved': RAE_saved,
        'Flickr_saved': Flickr_saved,
        'word_image': url_imagen,
        'imagen_meme': Meme,
        'definicion': definicion,
        'comentario_list': comentarios_list,
        'enlace_list': enlaces_list,
        'num_votos': num_votos,
        'voted': voted,
        'more_voted': ordered_list,
        'word': palabra,
        'word_exist': word_exist,
        'num_pals': num_pals,
        'rand': rand
    }

    return render(request,  "cms/palabra.html", context)


def index(request):
    num_pals = length_list_words()
    rand = random_words()
    cuenta = False
    page = int(request.GET.get('page', 1))
    lista = new_words(request, page)
    format = request.GET.get('format', None)
    if format:
        if format == "xml":
            data = serializers.serialize("xml", Palabra.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif format == "json":
            data = serializers.serialize("json", Palabra.objects.all())
            return HttpResponse(data, content_type='text/json')
    if request.method == "POST":
        value = check_forms(request)
        x = value[0]
        if x:
            return redirect(x)
    ordered_list = more_voted()
    context = {
        'more_voted': ordered_list,
        'cuenta': cuenta,
        'num_pals': num_pals,
        'rand': rand,
        'actual': str(page),
        'lista': lista,
    }

    return render(request, 'cms/inicio.html', context)