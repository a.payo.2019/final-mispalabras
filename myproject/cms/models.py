from django.db import models
from django.contrib.auth.models import User


class Palabra(models.Model):
    palabra = models.TextField(default="Palabra")
    valor = models.TextField(default="")
    definicion = models.TextField(default="")
    imagen = models.URLField(default="")
    autor = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    Meme = models.URLField(default="")
    fecha = models.DateTimeField(default="")
    rae = models.TextField(default="")
    Flickr = models.URLField(default="")

class Comentarios(models.Model):
    comentario = models.TextField(default="Comentario")
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.TextField(blank=False)
    fecha = models.DateTimeField(default="")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)

class Votos(models.Model):
    voto = models.TextField(default="Voto")
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.IntegerField(default=0)
    fecha = models.DateTimeField(default="")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)

class Urls(models.Model):
    url = models.TextField(default="Enlace")
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.URLField(blank=False)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha = models.DateTimeField(default="")
    descripcion = models.TextField(default="")
    imagen = models.URLField(blank=False)



