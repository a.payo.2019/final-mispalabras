from django.urls import path
from . import views
from django.contrib import admin

urlpatterns = [
    path('', views.index),
    path('register/', views.register),
    path('login/', views.login),
    path('logout/', views.loggout),
    path('delete/', views.delete_user),
    path('help/', views.help),
    path('users/<str:user>/', views.cuenta),
    path('<str:palabra>/', views.words),
]
